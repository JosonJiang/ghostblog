var path = require('path'),
config;



// ### Production

// config = {
// 			production: {
// 				url: 'http://JosonBlog.com', //替换为你自己的域名
// 				mail: { },
// 				database: {
// 							client: 'mysql',
// 							connection: 
// 							{
// 									host: '127.0.0.1',
// 									user     : 'root', //我们暂且用 MySQL 的 root 账户
// 									password : '123456', //输入你的 MySQL 密码
// 									database : 'ghost', //我们前面为 Ghost 创建的数据库名称
// 									charset  : 'utf8'
// 							}
// 						},
// 				server: 
// 				{
			
// 					host: '127.0.0.1',
// 					port: '2368'
// 				}
// 		}
// }


config={
	production:{
		url:'JosonBlog.com',
		mail:{},
		database:{
			client:'mysql',
			connection:{
				host:'db',
				user:'ghost',
				password:'ghost',
				database:'ghost',
				port:'3306',
				charset:'utf8'
			},
			debug:false
		},
		paths:{
			contentPath:path.join(process.env.GHOST_CONTENT,'/')
		},
		server:{
		    host:'0.0.0.0',
			port:'2368'
		}
	}
};
module.sssxports=config;

